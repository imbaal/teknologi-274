module.exports = exports = (app, pool) => {
    app.get('/api/variant', (request, response) => {
        const query = "select v.id,v.variant_name, v.variant_code, c.category_name from variant v join category c on c.category_id = c.category_code where v.is_active = true order by variant_code";
        pool.query(query, (error, result) => {
            console.log(result);
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/variant/:id', (request, response) => {
        const ids = request.params.id;
        const query = 'select * from variant where id = ' + ids;

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        })



    })
    app.post('/api/addvariant', (request, response) => {
        const { variant_code, variant_name, category_id} = request.body
        const query = `insert into public.variant(
            variant_code, variant_name, category_id, created_by, created_date,updated_date,is_active )
            values ('${variant_code}','${variant_name}','${category_id}','user1','now()','now',true)`;
        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data variant ${variant_name} saved`
                });
            }
        })
    })

    app.put('/api/updatevariant/:id', (request, response) => {
        const { id } = request.params
        const { variant_code, variant_name } = request.body;

        const query = `update public.variant set 
        variant_code = '${variant_code}',    
        variant_name = '${variant_name}',
            
            updated_date = 'now()'
            where id = ${id}`;

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data  ${variant_name}has been update`
                })
            }
        })
    })

    app.put('/api/deletevariant/:id', (request, response) => {
        const { id } = request.params

        const query = `update public.variant set 
        is_active = false,
        updated_date = 'now()',
        created_by = 'user1'
        where id = ${id}`;

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data has been deleted`
                })
            }
        })
    })

};