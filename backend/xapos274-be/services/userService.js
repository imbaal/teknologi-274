const { response, request } = require("express")

module.exports= exports =(app,pool)=>{
    app.get('/api/users', (request, response) => {
        pool.query('select * from users order by name ', (error, result) => {
            if (error) {
                throw error
            }
            else {
                response.status(200).json(result.rows)
            }
        })
    })
    
    app.put('/api/editusers/:id',(request,response) => {
        const { name,email } = request.body
        const id = parseInt(request.params.id)
        const query = `UPDATE users set name = '${name}', email = '${email}' where id = '${id}'`
        pool.query(query,(error,result)=>{
            if(error){
                response.send(400,{
                    success: false,
                    result: error
                })
            }else{
                response.send(201,{
                    success:true,
                    result:`Data ${name} saved`
                })
            }
        })
    })



    app.post ('/api/addusers',(request,response) => {
        const { name,email } = request.body
        const query = `INSERT INTO public.users(name,email)
                        values('${name}','${email}')`
        pool.query(query,(error,result)=>{
            if(error){
                response.send(400,{
                    success: false,
                    result: error
                })
            }else{
                response.send(201,{
                    success:true,
                    result:`Data ${name} saved`
                })
            }
        })
    })



    app.delete('/api/deleteusers/:id',(request,response) => {
        const { name,email } = request.body
        const id = parseInt(request.params.id)
        const query = `delete from users where id = '${id}'`
        pool.query(query,(error,result)=>{
            if(error){
                response.send(400,{
                    success: false,
                    result: error
                })
            }else{
                response.send(201,{
                    success:true,
                    result:`Data ${name} saved`
                })
            }
        })
    })


  
}

