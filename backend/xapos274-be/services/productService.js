module.exports = exports = (app, pool) => {
    app.get('/api/product', (request, response) => {
        const query = `select * from product p
        join variant v
        on v.id = p.variant_id
        join category c
        on v.category_id = c.id
        where p.is_active = true`;
        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        })
    })

    app.get('/api/product/:id', (request, response) => {
        const ids = request.params.id;
        const query = 'select * from product where id= '+ids;

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        })
    })

    app.post('/api/addproduct', (request, response) => {
        const { product_code, product_name, product_price, product_stock,} = request.body
        const query = `insert into public.product (
            product_code, product_name, product_price, product_stock,  created_by, created_date, is_active)
            values 
            ( '${product_code}', '${product_name}', '${product_price}', '${product_stock}', 'user1', 'now()', true)`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Product ${product_name} saved`
                })
            }
        })
    })


    app.put('/api/updateproduct/:id', (request, response) => {
        const { id } = request.params
        const { product_code, product_name, product_price, product_stock} = request.body;

        const query = `update public.product set
        product_code = '${product_code}',
        product_name = '${product_name}',
        product_price = '${product_price}',
        product_stock = '${product_stock}'
        where id ='${id}'`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200,{
                    success: true,
                    result: `Data ${product_name} has been updated`
                })
            }
        })
    })

    app.put ('/api/deleteproduct/:id',(request, response)=>{
        const {id} = request.params
        console.log(id);
        const query = `update public.product set
        is_active = false,
        updated_by= 'user1',
        updated_date = 'now()'
        where id = ${id}`;

        pool.query(query,(error,result)=>{
            if (error){
                response.send(400,{
                    success: false,
                    result:error,
                });
            }else{
                response.send(200,{
                    success:true,
                    result: `data has been deleted`
                })
            }
        })
    })
}