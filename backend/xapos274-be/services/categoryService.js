module.exports = exports = (app, pool) => {
    app.get('/api/category', (request, response) => {
        const query = "select * from category where is_active = true order by category_code" ;
        pool.query(query, (error, result) => {
            console.log(result);
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/category/:id',(request,response)=>{
        const ids = request.params.id;
        const query = 'select * from category where id = '+ids;

        pool.query(query,(error,result)=>{
            if(error){
                response.send(400,{
                    success :false,
                result:error,
                })
            }else {
                response.send(200,{
                    success : true,
                    result :result.rows[0]
                })
            }
        })



    })
    app.post('/api/addcategory', (request, response) => {
        const { category_code, category_name, } = request.body
        const query = `insert into public.category(
        category_code, category_name, create_by, create_date,is_active)
        values ('${category_code}','${category_name}','user1','now()',true)`;
        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data category ${category_name} saved`
                });
            }
        })
    })
    
    app.put('/api/updatecategory/:id', (request,response)=>{
        const {id} = request.params
        const {category_code, category_name} = request.body;

        const query = `update public.category set 
            category_name = '${category_name}',
            category_code = '${category_code}',
            modify_date = 'now()',
            modify_by = 'user1'
            where id = ${id}`;

        pool.query(query,(error,result)=>{
            if(error) {
                response.send(400, {
                    success : false,
                    result : error,
                });
            }else{ response.send(200,{
                success : true,
                result :`Data  ${category_name}has been update`
            })
            }
        })
    })

    app.put('/api/deletecategory/:id', (request,response)=>{
        const {id} = request.params

        const query = `update public.category set 
            is_active = false,
            modify_date = 'now',
            modify_by = 'user1'
            where id = ${id}`;

        pool.query(query,(error,result)=>{
            if(error) {
                response.send(400, {
                    success : false,
                    result : error,
                });
            }else{ response.send(200,{
                success : true,
                result :`Data has been deleted`
            })
            }
        })
    })

};