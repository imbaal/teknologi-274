const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
global.config = require('./config/dbconfig')


const app = express()
const port = 3000

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' })
})

require('./services/categoryService')(app,global.config.pool)
require('./services/variantService')(app,global.config.pool)
require('./services/userService')(app, global.config.pool)
require('./services/productService')(app,global.config.pool)

app.listen(port, () => {
    console.log(`App Running on port ${port}`)
})

